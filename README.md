# Version
- Keycloak 24.0
- Postgres 15.6 : [Keycloak Supported databases](https://www.keycloak.org/server/db)
- pro sante connect connector 4.0.0 : [Releases](https://github.com/ansforge/keycloak-prosanteconnect/releases)


# Setting Up Server

## Configure environnement variables

- copy ```.envrc``` file and rename it ```.env```
- fill ```tofill``` fields like  password (keycloak, postgres and openldap)

## Remote server

### Configure DNS entries

### Run nginx configuration

- keycloack
```nginx
server {
    server_name keycloak-test.interhop.org;
    listen 80;

    location / {
        proxy_pass http://127.0.0.1:9092;

    	proxy_set_header X-Forwarded-For $proxy_protocol_addr; # To forward the original client's IP address
    	proxy_set_header X-Forwarded-Proto $scheme; # to forward the  original protocol (HTTP or HTTPS)
    	proxy_set_header Host $host; # to forward the original host requested by the client

        access_log  /var/log/nginx/keycloak.log;
        error_log   /var/log/nginx/keycloak.error.log;

    }
}
```
- phlpldapadmin ou adminer
```nginx
server {

        server_name phpldapadmin-test.interhop.org;
	listen 80 ;

	location / {
		proxy_pass http://127.0.0.1:9091$request_uri;
		proxy_set_header Host $host;

         access_log  /var/log/nginx/phpldapmyadmin.log;
        error_log   /var/log/nginx/phpldapmyadmin.error.log;
	}
}
```

### Run Cerbot

```certbot --nginx```

### Run Docker compose
```docker compose up -d```

# Access Web Interfaces

## keycloack

- url : cf nginx file
- user: admin
- pwd: ``${KEYCLOAK_PWD}``


## adminer

- url : cf nginx file
- serveur : postgres
- user: my_keycloak
- pwd: ``${POSTGRES_PWD}``
- database : keycloak

## phpladapmyadmin:
- url : cf nginx file
https://computingforgeeks.com/run-openldap-server-in-docker-containers/?expand_article=1
- login DN : cn=admin,dc=interhop,dc=org
- pwd: ``${LDAP_PWD}``



# Setting up Pro Sante Connect : "Identity Provider"

Documentation technique : https://industriels.esante.gouv.fr/produits-et-services/pro-sante-connect/documentation-technique

Dépot de code : https://github.com/ansforge/keycloak-prosanteconnect

Référentiel PSC : https://industriels.esante.gouv.fr/sites/default/files/media/document/ANS_Referentiel_20210615_v1.8.pdf

Services raccordés https://esante.gouv.fr/offres-services/e-cps/services-raccordes-a-pro-sante-connect

Pour corriger le centrage du logo : https://github.com/ansforge/keycloak-prosanteconnect/issues/24

# Setting Up Email Server

Riseup Configuration : https://riseup.net/fr/email/settings/smtp

Keycloak Configuration : https://www.keycloak.org/docs/latest/server_admin/#_email


## Set Reaml Setting > `Email`

![](https://pad.interhop.org/uploads/20331817-29fd-48fe-abfa-45219643a501.png)


![](https://pad.interhop.org/uploads/82c2332c-507c-428b-a40c-c33167ee239c.png)

## Put `Verify Email` to True

![](https://pad.interhop.org/uploads/2a33f386-da29-40eb-818c-2a25205f2bc9.png)

# Setting up Client

## For React App

![](https://pad.interhop.org/uploads/a2ea752f-fabc-4d7f-ba8b-1490712a6c5a.png)
- Client authentificaiton : off
  - not useful for front-end applications.
  - Generates a secret-id
- Exchange method : S256


Source : [Securing a React App using Keycloak](https://medium.hexadefence.com/securing-a-react-app-using-keycloak-ac0ee5dd4bfc)

## Admin API

- Online Postman : https://documenter.getpostman.com/view/7294517/SzmfZHnd#6d0e3fcf-3d27-4878-a157-ae617588ac89
- https://www.keycloak.org/docs-api/24.0.2/rest-api/index.html#_version_information
- Example : [Keycloak Admin REST API](https://medium.hexadefence.com/keycloak-admin-rest-api-63a294814e1b)

## Account Management API
- Not officially documented
- Source code : https://github.com/keycloak/keycloak/blob/main/services/src/main/java/org/keycloak/services/resources/account/AccountRestService.java
- Unofficial openAPI documentation (cf account-management-api.yml): 
  - `Source` : https://gist.github.com/xgp/2d77cbebc6164160faae6aa77d127a57
  - Adapté depuis `Source` (avec url du serveur de test) : [account-management-api.yml](https://framagit.org/interhop/toobib/keycloak/-/blob/master/account-management-api.yml)

# Ajout attribut
https://www.baeldung.com/keycloak-custom-user-attributes

## Ajouter un attribut
![](https://pad.interhop.org/uploads/2db00789-88a0-43ed-abae-17d8371415f1.png)

## Aller dans client>client-scope>{{client}}-dedicated
![](https://pad.interhop.org/uploads/1e423f0b-42f0-48aa-9537-4643458efc3a.png)

## Add Mapper by configuration 

- user Attribute

![](https://pad.interhop.org/uploads/df885fc0-3d7b-4bc6-a2bd-c7cd991bb790.png)

![](https://pad.interhop.org/uploads/a9998e58-c71b-48b6-a600-efbaa5d8608a.png)



# Export / Import Realm
- https://github.com/keycloak/keycloak/issues/20442
- https://www.keycloak.org/server/importExport
- https://www.keycloak.org/server/containers
- https://www.linkedin.com/pulse/keycloak-docker-container-how-exportimport-realm-henry-xiloj-herrera/

```bash
docker exec -u root -t -i <container_id> /bin/bash
```
```bash
/opt/keycloak/bin/kc.sh export --dir /opt/keycloak/data/import --realm Toobib --users realm_file
```

Sortir du container

```bash
docker cp <container-id>:/opt/keycloak/data/import/Toobib-realm.json import/realm/Toobib-realm.json
```

# Setting up LDAP (if needed)

## PHPLDAPAdmin

An organizational unit ``users`` and two users ```aparrot``` and ```parisni``` are already created from the file ```ldifs/initiation.ldif```

(```slappasswd``` command may be used to create pwd)
 

##  Keycloak

### Configure User Federation

####  Click on `User Federation`, choose `Add LDAP providers`

#### Entry the following configuration
```yaml=
Vendor: Other

Connection URL: ldap://openldap:389

Bind type: simple
Bind DN: cn=admin,dc=interhop,dc=org
Bind credential: ldappwd
Edit mode :READ_ONLY # allows to remove user from ldap, optionnal
User DN: ou=users,dc=interhop,dc=org
Username LDAP attribute: uid ~~(cn)~~
RDN LDAP attr: uid ~~(cn)~~
UUID LDAP attr: uid
User object classes: inetOrgPerson, organizationalPerson, person, top ~~posixAccount, inetOrgPerson, top~~
Search scope: One Level ~~Subtree~~
```

![](https://pad.interhop.org/uploads/f3509688-f7bc-4d42-b37b-201f101e2311.png)


![](https://pad.interhop.org/uploads/2b94da23-08b0-4cbc-9509-fedfbafc08e7.png)


![](https://pad.interhop.org/uploads/98ec7b72-8361-4cfe-8d80-41ef175d7771.png)


![](https://pad.interhop.org/uploads/53db071e-4261-442c-ab72-216f468b6a03.png)


![](https://pad.interhop.org/uploads/49aad579-0adf-4147-9da4-77d6a7e37573.png)


####  Go to `mappers` and `add mapper`

! Les groupes doivent être définis en LDAP avant : https://framagit.org/interhop/toobib/keycloak/-/blob/master/ldifs/initiation.ldif?ref_type=heads

![](https://pad.interhop.org/uploads/7bde7789-2f88-4fda-b77e-738283c9dd67.png)

#### Entry the following configuration

```yaml=
Name: groups
Mapper type: group-ldap-mapper
LDAP group dn: ou=groups,dc=interhop,dc=org
Group Name LDAP attribute: cn
Group Object Classes: posixGroup
Preserver group inherit: on
Membership LDAP Attribute: memberuid
Membership Attribute Type: DN
Membership User LDAP Attribute: cn
mode: LDAP_ONLY #-> allows to remove group from ldap
user group retrieve strategy: LOAD_GROUPS_BY_MEMBER_ATTR
```


![](https://pad.interhop.org/uploads/3bf7c5c7-3247-41a8-99ce-f99f809b5035.png)

![](https://pad.interhop.org/uploads/3532bde0-bb6a-4163-9bf0-b2b4119e2eba.png)

![](https://pad.interhop.org/uploads/46d44885-09aa-4527-a953-677855b3d5c7.png)

### Create Client
![](https://pad.interhop.org/uploads/277fced3-122f-410f-ab7b-71104abb717d.png)

#### To create group - client liaison

1. creer un nouveau client : test-openid-connect
2. creer un mapper : client scope > test-openid-connect-dedicated > add mapper (by configuration) > group membership
 
![](https://pad.interhop.org/uploads/053d7f55-cf2e-4540-bb2d-79a8ea64e066.png)

![](https://pad.interhop.org/uploads/c5954354-2384-44bd-9fe7-8d93cac14739.png)
